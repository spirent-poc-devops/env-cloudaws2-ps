module "mgmt_station_instance" {
  source                        = "git::https://github.com/cloudposse/terraform-aws-ec2-instance-group.git?ref=master"
  instance_count                = "1"
  ami                           = var.ami
  ami_owner                     = var.ami_owner
  name                          = "${var.env_prefix}-mgmt-station"
  create_default_security_group = true
  security_groups               = [var.sg_id]
  region                        = var.aws_region
  subnet                        = var.subnet
  vpc_id                        = var.vpc
  associate_public_ip_address   = true
  generate_ssh_key_pair         = true
  assign_eip_address            = false 
  ssh_key_pair_path             = var.ssh_key_pair_path
  instance_type                 = var.instance_type
  root_volume_size              = var.root_volume_size
  tags = {
    "Description"    = "Mgmt Station Server"
    "dlm_daily_prod" = "true"
    "OS"             = "Ubuntu Server 18.04 LTS"
  }
}

resource "aws_iam_role_policy_attachment" "administartion_access" {
  role       = module.mgmt_station_instance.role_names[0]
  policy_arn = "arn:aws:iam::aws:policy/AdministratorAccess"
}
