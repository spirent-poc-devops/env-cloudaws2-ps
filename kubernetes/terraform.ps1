#!/usr/bin/env pwsh

param
(
    [Parameter(Mandatory = $true, Position = 0)]
    [hashtable] $Config,

    [Parameter(Mandatory = $true, Position = 1)]
    [hashtable] $Resources
)

$kubernetesTerraformPath = "$PSScriptRoot/../temp/kubernetes_$(Get-EnvMapValue -Map $config -Key "environment.prefix")"
if (!(Test-Path $kubernetesTerraformPath)) {
    $null = New-Item $kubernetesTerraformPath -ItemType "directory"
}

$env:TF_VAR_aws_region = Get-EnvMapValue -Map $config -Key "aws.region"
$env:TF_VAR_aws_credentials_file = Get-EnvMapValue -Map $config -Key "aws.credentials_file"
$env:TF_VAR_aws_profile = Get-EnvMapValue -Map $config -Key "aws.profile"
$env:TF_VAR_aws_vpc = Get-EnvMapValue -Map $config -Key "aws.vpc"
$env:TF_VAR_env_nameprefix = Get-EnvMapValue -Map $config -Key "environment.prefix"
$env:TF_VAR_k8s_version = Get-EnvMapValue -Map $config -Key "k8s.version"
$env:TF_VAR_k8s_desired_nodes = Get-EnvMapValue -Map $config -Key "k8s.desired_nodes"
$env:TF_VAR_k8s_min_nodes = Get-EnvMapValue -Map $config -Key "k8s.min_nodes"
$env:TF_VAR_k8s_max_nodes = Get-EnvMapValue -Map $config -Key "k8s.max_nodes"
$env:TF_VAR_k8s_instance_type = Get-EnvMapValue -Map $config -Key "k8s.instance_type"
$subnets = "[`"$($(Get-EnvMapValue -Map $config -Key "k8s.subnets") -join "," -replace ",",'", "' )`"]"
$env:TF_VAR_k8s_subnets=$subnets

Copy-Item -Path "$PSScriptRoot/templates/k8s.tf" -Destination "$kubernetesTerraformPath/k8s.tf"

# Load terraform state from config folder
Sync-State -Component "eks" -EnvPrefix $(Get-EnvMapValue -Map $config -Key "environment.prefix")
