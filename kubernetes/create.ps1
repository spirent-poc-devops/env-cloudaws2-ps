#!/usr/bin/env pwsh

param
(
    [Parameter(Mandatory = $true, Position = 0)]
    [string] $ConfigPath,

    [Parameter(Mandatory = $true, Position = 1)]
    [string] $ResourcePath
)

# Read config and resources
$config = Read-EnvConfig -ConfigPath $ConfigPath
$resources = Read-EnvResources -ResourcePath $ResourcePath

# Prepare terraform files and set terraform env vars
. "$PSScriptRoot/terraform.ps1" -Config $config -Resources $resources

# Initialize terraform
Set-Location $kubernetesTerraformPath
Write-Host "Initializing terraform..."
terraform init
# Check for error
if ($LastExitCode -ne 0) {
    Set-Location "$PSScriptRoot/.."
    Write-EnvError -Component "kubernetes" "Can't initialize terraform. Watch logs above or check '$kubernetesTerraformPath' folder content."
}

# Plan cloud resources creation
Write-Host "Executing terraform plan command..."
terraform plan
# Check for error
if ($LastExitCode -ne 0) {
    Set-Location "$PSScriptRoot/.."
    Write-EnvError -Component "kubernetes" "Can't execute terraform plan. Watch logs above or check '$kubernetesTerraformPath' folder content."
}

# Create cloud resources
Write-Host "Creating cloud resources by terraform..."
terraform apply -auto-approve
# # Check for error
# if ($LastExitCode -ne 0) {
#     Set-Location "$PSScriptRoot/.."
#     Write-EnvError -Component "kubernetes" "Can't create cloud resources. Watch logs above or check '$kubernetesTerraformPath' folder content."
# }

#EKS Cluster Outputs
$EKSClusterID = $(terraform output eks_cluster_id).Trim('"')
$EKSClusterArn = $(terraform output eks_cluster_arn).Trim('"')
$EKSClusterEndpoint = $(terraform output eks_cluster_endpoint).Trim('"')

Set-Location -Path "$PSScriptRoot/.."

Set-EnvMapValue -Map $resources -Key "k8s.cluster_name" -Value $EKSClusterID
Set-EnvMapValue -Map $resources -Key "k8s.cluster_arn" -Value $EKSClusterArn
Set-EnvMapValue -Map $resources -Key "k8s.cluster_endpoint" -Value $EKSClusterEndpoint

# Save terraform state to config folder
Save-State -Component "kubernetes" -EnvPrefix $(Get-EnvMapValue -Map $config -Key "environment.prefix")

Write-EnvResources -ResourcePath $ResourcePath -Resources $resources

# Write-Host "Waiting for a minutes for EKS worker nodes to get ready..." -ForegroundColor Green
# Start-Sleep -Seconds 60

aws eks update-kubeconfig --region (Get-EnvMapValue -Map $config -Key "aws.region") --name $EKSClusterID
