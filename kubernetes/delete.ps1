#!/usr/bin/env pwsh

param
(
    [Parameter(Mandatory = $true, Position = 0)]
    [string] $ConfigPath,

    [Parameter(Mandatory = $true, Position = 1)]
    [string] $ResourcePath
)

# Read config and resources
$config = Read-EnvConfig -ConfigPath $ConfigPath
$resources = Read-EnvResources -ResourcePath $ResourcePath

# Prepare terraform files and set terraform env vars
. "$PSScriptRoot/terraform.ps1" -Config $config -Resources $resources

# Delete aws kubernetes resources
Set-Location $kubernetesTerraformPath
terraform init
# Check for error
if ($LastExitCode -ne 0) {
    Set-Location "$PSScriptRoot/.."
    Write-EnvError -Component "kubernetes" "Can't initialize terraform. Watch logs above or check '$kubernetesTerraformPath' folder content."
}

terraform plan
# Check for error
if ($LastExitCode -ne 0) {
    Set-Location "$PSScriptRoot/.."
    Write-EnvError -Component "kubernetes" "Can't execute terraform plan. Watch logs above or check '$kubernetesTerraformPath' folder content."
}

terraform destroy -auto-approve
# Check for error
if ($LastExitCode -ne 0) {
    Set-Location "$PSScriptRoot/.."
    Write-EnvError -Component "kubernetes" "Can't delete cloud resources. Watch logs above or check '$kubernetesTerraformPath' folder content."
}

Set-Location "$PSScriptRoot/.."

# Delete terraform state from config folder
Remove-State -Component "kubernetes" -EnvPrefix $(Get-EnvMapValue -Map $config -Key "environment.prefix")

if (Test-EnvMapValue -Map $resources -Key "k8s") {
    Remove-EnvMapValue -Map $resources -Key "k8s.cluster_name"
    Remove-EnvMapValue -Map $resources -Key "k8s.cluster_arn"
    Remove-EnvMapValue -Map $resources -Key "k8s.cluster_endpoint"
}

Write-EnvResources -ResourcePath $ResourcePath -Resources $resources
